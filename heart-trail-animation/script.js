const bodyEl = document.querySelector("body");

bodyEl.addEventListener("mousemove", (event) => {
  const xPos = event.offsetX;
  const yPos = event.offsetY;
  const spanEl = document.createElement("span");
  spanEl.style.left = xPos + "rem";
  spanEl.style.top = yPos + "rem";
  const size = Math.random() * 100;
  spanEl.style.width = size + "rem";
  spanEl.style.height = size + "rem";
  bodyEl.appendChild(spanEl);
  setTimeout(() => {
    spanEl.remove();
  }, 3000);
});